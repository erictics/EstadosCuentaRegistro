﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Diagnostics.Eventing;
using System.ServiceModel;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Configuration;
using System.ServiceModel.Web;
using System.Net;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace SoftvWCFService
{
    public class MessageInspector : IDispatchMessageInspector, IServiceBehavior
    {
        #region IDispatchMessageInspector
        List<String> lstInvaliModules;
        List<String> lstInvaliAction;
        public MessageInspector()
        {
            lstInvaliModules = ConfigurationManager.AppSettings["NoRegisterInBitacoraModules"].Split(',').ToList();
            lstInvaliAction = ConfigurationManager.AppSettings["NoRegisterInBitacoraStartWith"].Split(',').ToList();
        }

        public static XmlDocument RemoveXmlns(String xml)
        {
            XDocument d = XDocument.Parse(xml);
            d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();
            d.Root.Descendants().Attributes().Where(x => x.Name.Namespace != "").Remove();
            foreach (var elem in d.Descendants())
                elem.Name = elem.Name.LocalName;
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(d.CreateReader());
            return xmlDocument;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Remove("Access-Control-Allow-Methods");
            WebOperationContext.Current.OutgoingResponse.Headers.Remove("Access-Control-Allow-Origin");
            WebOperationContext.Current.OutgoingResponse.Headers.Remove("Access-Control-Allow-Headers");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "*");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Authorization,accept,content-type");
        }     
       
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                if (WebOperationContext.Current.IncomingRequest.Headers["Authorization"] == null)
                {
                    //WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"myrealm\"");
                    throw new WebFaultException<string>("Acceso no autorizado, favor de validar autenticación", HttpStatusCode.Unauthorized);
                }
                else // Decode the header, check password
                {

                }
            }



            return null;
        }     
      


        #endregion

        #region IServiceBehavior

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (var endpoint in dispatcher.Endpoints)
                {
                    endpoint.DispatchRuntime.MessageInspectors.Add(new MessageInspector());
                }
            }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        public void Validate(ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }
}