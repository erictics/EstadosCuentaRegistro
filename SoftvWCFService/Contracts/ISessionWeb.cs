﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Softv.Entities;
namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISessionWeb
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetvalidaAparato", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        InfoCliente GetvalidaAparato(int ? id,string serie);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getregistracliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? Getregistracliente(long? contrato, string login, string pasaporte);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaRecoverPassword", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long ? GetValidaRecoverPassword(int? id, string serie, string email);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpdateCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetUpdateCliente(string correo, string password, long? contrato, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "validateEmail", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool? validateEmail(long ? contrato, string email);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetregistraclienteToolbox", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetregistraclienteToolbox(long? contrato, string login, string pasaporte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUpdateClienteToolbox", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetUpdateClienteToolbox(string correo, string password, long? contrato, int? op);

    }
}

