﻿
using Microsoft.IdentityModel.Tokens;
using SoftvWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using Softv.Entities;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService:ISessionWeb      
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;


        public InfoCliente GetvalidaAparato(int? id,string serie)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                InfoCliente i = new InfoCliente();
                i = ObtieneClientePorSerie(serie);

                if (i.Contrato > 0)
                {
                    try
                    {                       
                        using (SqlConnection connection = new SqlConnection(ConnectionString))
                        {
                            SqlCommand command = new SqlCommand("exec SP_validaUsuarioRegistrado @contrato", connection);
                            command.Parameters.AddWithValue("@contrato", i.Contrato);

                            IDataReader rd = null;
                            
                            try
                            {
                                if (connection.State == ConnectionState.Closed)
                                    connection.Open();
                                rd = command.ExecuteReader();
                                while (rd.Read())
                                {  
                                    if (bool.Parse(rd[0].ToString()) == true)
                                    {
                                        i.valid = false;
                                    }else
                                    {
                                        i.valid = true;
                                    }
                                    
                                }

                                i.message = (i.valid == true) ? "La serie ingresada es correcta" :"ya existe un usuario registrado con la serie proporcionada";
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                            }
                            finally
                            {
                                if (connection != null)
                                    connection.Close();
                                if (rd != null)
                                    rd.Close();
                            }
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                    }

                }else
                {
                    i.message = "El numero de serie no esta relacionado a un cliente ,Intente ingresando otra serie";
                }
                
                return i;
            }
        }


        public InfoCliente ObtieneClientePorSerie(string serie)
        {
            InfoCliente i = new InfoCliente();
            i.Contrato = 0;
            i.valid = false;
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand command = new SqlCommand("exec ObtieneClientePorSerie @Serie", connection);
                    command.Parameters.AddWithValue("@Serie", serie);

                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            i.Contrato = long.Parse(rd[0].ToString());
                            i.ContratoCompuesto = rd[1].ToString();
                            i.Cliente = rd[2].ToString();
                        }



                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
            }

            return i;
        }


        public long? GetValidaRecoverPassword(int? id, string serie, string email)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                bool existe = false;

                InfoCliente i = new InfoCliente();
                i = ObtieneClientePorSerie(serie);

                if (i.Contrato != null && i.Contrato > 0)
                {

                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("exec SP_ValidaRecoverPassword @email,@contrato", connection);
                        command.Parameters.AddWithValue("@email", email);
                        command.Parameters.AddWithValue("@contrato", i.Contrato);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                existe = bool.Parse(rd[0].ToString());

                            }
                            if (existe == false)
                            {
                                i.Contrato = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }
                    }
                }
                else
                {
                    return 0;
                }
                return i.Contrato;
            }
        }


        public bool? validateEmail(long ? contrato,string email)
        {
            bool existe = false;
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("exec SP_validateEmail @email,@contrato", connection);
                        command.Parameters.AddWithValue("@email", email);
                        command.Parameters.AddWithValue("@contrato", contrato);

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                existe = bool.Parse(rd[0].ToString());

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

                
            }
            return existe;
        }

        public int ? GetUpdateCliente(string correo,string password,long ? contrato,int ? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("exec SP_updateregistro @correo ,@password,@contrato,@op", connection);
                        command.Parameters.AddWithValue("@correo", correo);
                        command.Parameters.AddWithValue("@password", password);
                        command.Parameters.AddWithValue("@contrato", contrato);
                        command.Parameters.AddWithValue("@op", op);
                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        try
                        {
                            if (result == 1)
                            {
                                SendEmail(correo, ObtenPasswordEmail(correo,contrato));
                            }

                        }
                        catch { }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }
        
        public int ? Getregistracliente(long ? contrato, string login,string pasaporte)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;                    
                    using (SqlConnection connection = new SqlConnection(ConnectionString))                    {
                        SqlCommand command = new SqlCommand("exec SP_registraUsuarioEdocta @contrato ,@login, @pasaporte", connection);
                        command.Parameters.AddWithValue("@contrato", contrato);
                        command.Parameters.AddWithValue("@login", login);
                        command.Parameters.AddWithValue("@pasaporte", pasaporte);
                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        try
                        {
                            if (result==1)
                            {
                                SendEmail(login, pasaporte);
                            }
                            
                        }
                        catch { }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public static void SendEmail(string login,string pasaporte)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("noreplystartv@gmail.com", "0601x-2L");

            string output = null;
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(login));
            email.From = new MailAddress("noreplystartv@gmail.com");
            email.Subject = "Star TV- Credenciales de acceso";
            email.Body = emailBody().Replace("replaceemail", login).Replace("replacepassword",pasaporte);

            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            try
            {
                smtp.Send(email);
                email.Dispose();
                output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
        }

        public string ObtenPasswordEmail(string correo ,long ? contrato)
        {           
            string password = "";
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("exec EstadosCuentaObtenPassword @correo,@contrato", connection);
                command.Parameters.AddWithValue("@correo", correo);               
                command.Parameters.AddWithValue("@contrato", contrato);                
                IDataReader rd = null;

                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = command.ExecuteReader();
                    while (rd.Read())
                    {
                        password = rd[0].ToString();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Error EstadosCuentaObtenPassword " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }

            }
            return password;
        }
                

        public static string emailBody()
        {
            string Body = "";
            try
            {
               Body = System.IO.File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplate/email.htm");
            }
            catch (Exception e)
            {

            }
            

            return Body;
        }




        public int? GetregistraclienteToolbox(long? contrato, string login, string pasaporte)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("exec SP_registraUsuarioEdocta @contrato ,@login, @pasaporte", connection);
                        command.Parameters.AddWithValue("@contrato", contrato);
                        command.Parameters.AddWithValue("@login", login);
                        command.Parameters.AddWithValue("@pasaporte", pasaporte);
                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        try
                        {
                            if (result == 1)
                            {
                                SendEmailToolBox(login, pasaporte);
                            }

                        }
                        catch { }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public static void SendEmailToolBox(string login, string pasaporte)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("noreplystartv@gmail.com", "0601x-2L");

            string output = null;
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(login));
            email.From = new MailAddress("noreplystartv@gmail.com");
            email.Subject = "Star TV- Credenciales de acceso";
            email.Body = emailBodyToolbox().Replace("replaceemail", login).Replace("replacepassword", pasaporte);

            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            try
            {
                smtp.Send(email);
                email.Dispose();
                output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
        }

        public static string emailBodyToolbox()
        {
            string Body = "";
            try
            {
                Body = System.IO.File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplate/emailToolbox.htm");
            }
            catch (Exception e)
            {

            }


            return Body;
        }

        public int? GetUpdateClienteToolbox(string correo, string password, long? contrato, int? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))
                    {
                        SqlCommand command = new SqlCommand("exec SP_updateregistro @correo ,@password,@contrato,@op", connection);
                        command.Parameters.AddWithValue("@correo", correo);
                        command.Parameters.AddWithValue("@password", password);
                        command.Parameters.AddWithValue("@contrato", contrato);
                        command.Parameters.AddWithValue("@op", op);
                        InfoCliente i = new InfoCliente();
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                result = Int32.Parse(rd[0].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error Get usuario By User And Pass " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        try
                        {
                            if (result == 1)
                            {
                                SendEmailToolBox(correo, ObtenPasswordEmail(correo, contrato));
                            }

                        }
                        catch { }

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


    }
}
